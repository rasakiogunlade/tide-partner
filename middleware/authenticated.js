export default function ({ store, redirect }) {
  if (!store.getters['auth/isAuthenticated']) {
    return redirect('/auth/login')
  }
  // var user = store.getters['auth/user']

  // if (!user.is_verified) {
  //   return redirect('/otp')
  // }
}
