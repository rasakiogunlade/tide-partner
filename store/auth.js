import Axios from 'axios'
import { Store } from 'vuex'

export const state = () => ({
  token: null,
  authenticated: false,
  blur: false,
  user: [],
})

export const mutations = {
  SET_AUTH: (state, payload) => {
    state.authenticated = true
  },
  SET_USER: (state, payload) => {
    state.user = payload
  },
  setToken: (state, payload) => {
    state.token = payload
  },
  BLUR_PAGE: (state, payload) => {
    state.blur = payload
  },
  RESET_STATE(state, payload) {
    state.authenticated = payload
    window.localStorage.clear()
    this.$router.push('/auth/login')
  },
}

export const getters = {
  isAuthenticated(state) {
    return state.authenticated
  },
  token: (state) => {
    return state.token
  },
  user: (state) => {
    return state.user
  },
  blur: (state) => {
    return state.blur
  },
}

export const actions = {
  async register(
    { commit },
    { email, phone, first_name, last_name, password, password_confirmation }
  ) {
    const data = await this.$axios.$post('/auth/register', {
      email,
      phone,
      first_name,
      last_name,
      password,
      password_confirmation,
    })
    return data
  },
  async login({ commit }, { email, password }) {
    const data = await this.$axios.$post('/auth/login', {
      email,
      password,
    })
    this.$axios.defaults.headers.common['Token'] = data.data.access_token
    commit('setToken', data.data.access_token)
    return data
  },
  async forgotpassword({ commit }, { email, callback_url }) {
    const data = await this.$axios.$post('/auth/password/forgot', {
      email,
      callback_url,
    })
    return data
  },
  async verifyotp({ commit }, { verification_otp }) {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$post(
      '/auth/verify/otp',
      {
        verification_otp,
      },
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    )
    return data
  },
  async resendotp() {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$get('/auth/resend/otp', {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    return data
  },
  async logout({ commit }) {
    commit('RESET_STATE', false)
  },

  async resetpassword(
    { commit },
    { email, password, password_confirmation, token }
  ) {
    const data = await this.$axios.$post('/auth/password/reset', {
      email,
      password,
      password_confirmation,
      token,
    })
    return data
  },

  async profile() {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$get('/account/profile', {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    return data
  },
  async logs() {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$get('/account/logs', {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    return data
  },
  async updateprofile({ commit }, { email, first_name, last_name }) {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$patch(
      '/account/update-profile',
      {
        email,
        first_name,
        last_name,
      },
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    )
    return data
  },
  async changepassword(
    { commit },
    { new_password, new_password_confirmation }
  ) {
    let token = JSON.parse(window.localStorage.getItem('tide')).auth.token
    const data = await this.$axios.$patch(
      '/account/change-password',
      {
        new_password,
        new_password_confirmation,
      },
      {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      }
    )
    return data
  },
}
